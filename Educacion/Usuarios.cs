﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibre;

namespace Educacion
{
    public partial class Usuarios : formabase
    {
        public Usuarios()
        {
            InitializeComponent();
        }
       

        private void Usuarios_Load(object sender, EventArgs e)
        {
            
        }

        private void btnRegistro_Click(object sender, EventArgs e)
        {
            Guardar();
        }
        public Boolean Guardar()
        {
            string cmd = string.Format("EXEC ActualizaAlumno '{0}'", textBox1.Text.Trim());
            Util.Ejecutar(cmd);
            return true;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Eliminar();
        }
        public Boolean Eliminar()
        {
            string cmd = string.Format("EXEC EliminarAlumnos '{0}'", textBox1.Text.Trim());
            Util.Ejecutar(cmd);
            return true;

        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {
            VERalumnos ver = new VERalumnos();
            ver.ShowDialog();
        }
    }
}
