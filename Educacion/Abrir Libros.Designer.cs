﻿namespace Educacion
{
    partial class Abrir_Libros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTNaBRIR = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(102, 141);
            // 
            // BTNaBRIR
            // 
            this.BTNaBRIR.Location = new System.Drawing.Point(12, 99);
            this.BTNaBRIR.Name = "BTNaBRIR";
            this.BTNaBRIR.Size = new System.Drawing.Size(75, 23);
            this.BTNaBRIR.TabIndex = 0;
            this.BTNaBRIR.Text = "Abrir Libro 1";
            this.BTNaBRIR.UseVisualStyleBackColor = true;
            this.BTNaBRIR.Click += new System.EventHandler(this.BTNaBRIR_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(216, 99);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Abrir Libro  2";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(102, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Abrir Libro 3";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Abrir_Libros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 176);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BTNaBRIR);
            this.Name = "Abrir_Libros";
            this.Text = "Abrir_Libros";
            this.Load += new System.EventHandler(this.Abrir_Libros_Load);
            this.Controls.SetChildIndex(this.BTNaBRIR, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTNaBRIR;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}