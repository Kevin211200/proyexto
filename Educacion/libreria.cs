﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Educacion
{
    public partial class libreria : Component
    {
        public libreria()
        {
            InitializeComponent();
        }

        public libreria(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
