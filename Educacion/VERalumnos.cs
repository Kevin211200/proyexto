﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibre;

namespace Educacion
{
    public partial class VERalumnos : formabase
    {
        public VERalumnos()
        {
            InitializeComponent();
        }
        public DataSet LLenarDataGV(string tabla)
        {
            DataSet ds;
            string cmd = string.Format("Select * FROM " + tabla);
            ds = Util.Ejecutar(cmd);

            return ds;
        }

        private void VERalumnos_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LLenarDataGV("Alumno").Tables[0];
        }
    }
}
