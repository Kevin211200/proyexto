﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Educacion
{
    public partial class Menu : formabase
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Abrir_Libros Av = new Abrir_Libros();
            Av.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Usuarios Usu = new Usuarios();
            Usu.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Reproductor Re = new Reproductor();
            Re.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Navegador nA = new Navegador();
            nA.Show();
        }
    }
}
