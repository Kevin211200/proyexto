﻿namespace System
{
    internal class Url
    {
        private string v;
        private UriKind absolute;

        public Url(string v, UriKind absolute)
        {
            this.v = v;
            this.absolute = absolute;
        }
    }
}