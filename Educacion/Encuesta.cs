﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibre;

namespace Educacion
{
    public partial class Encuesta : Form
    {
        public Encuesta()
        {
            InitializeComponent();
        }
        public int si=0;
        public int no = 0;

        public int rest = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            rest = 1;
            si++;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            rest = 2;
            no++;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        public Boolean Guardar()
        {
            string cmd = string.Format("EXEC Result '{0}','{1}',{2}",rest,si,no);
            Util.Ejecutar(cmd);
            return true;

        }
    }
}
